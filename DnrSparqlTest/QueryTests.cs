﻿namespace DnrSparqlTest
{
    using System.Collections;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;

    using NUnit.Framework;

    using VDS.RDF;
    using VDS.RDF.Parsing;
    using VDS.RDF.Query;
    using VDS.RDF.Writing;

    [TestFixture]
    public class QueryTests
    {
        private ISparqlQueryProcessor _processor;
        private TripleStore _store;

        [SetUp]
        public void Setup()
        {
            _store = new TripleStore();
            _processor = new LeviathanQueryProcessor(_store);
        }

        [Test, Repeat(10)]
        public void Should_return_same_results_each_time()
        {
            // given
            LoadStore();
            var query = LoadQuery();

            // when
            var result = (SparqlResultSet)_processor.ProcessQuery(query);

            // then
            Assert.That(result.Count, Is.EqualTo(4));
        }

        [Timeout(5000)]
        [TestCaseSource("TimedDatasets")]
        public void Should_query_in_a_timely_manner(int datasetSize)
        {
            // given
            GenerateStore(datasetSize);
            var query = LoadQuery();

            // when
            var result = (SparqlResultSet)_processor.ProcessQuery(query);

            // then
            Assert.That(result.Count, Is.GreaterThan(0));
            Debug.WriteLine(result.Count);
        }

        [TestCaseSource("Queries")]
        public void Speed_test(string queryString)
        {
            const int UPPER = 100000;
            this.GenerateStore(10);
            var query = new SparqlQueryParser().ParseFromString(queryString);

            for (var i = 0; i < UPPER; i++)
            {
                var result = (SparqlResultSet)_processor.ProcessQuery(query);

                Assert.That(result, Has.Count.EqualTo(4));
            }
        }

        private SparqlQuery LoadQuery()
        {
            var parser = new SparqlQueryParser();
            var sparqlQuery = parser.ParseFromString(Resourcer.Resource.AsString("Query.rq"));
            sparqlQuery.Timeout = int.MaxValue;
            return sparqlQuery;
        }

        private void LoadStore()
        {
            _store.LoadFromString(Resourcer.Resource.AsString("SmallDataset.trig"), new TriGParser());

            Debug.WriteLine("Loaded {0} quads", _store.Triples.Count());
            _store.SaveToFile("data.trig", new TriGWriter());
        }

        private void GenerateStore(int graphCount)
        {
            for (int i = 0; i < graphCount; i++)
            {
                string format = Resourcer.Resource.AsString("Dataset.trig");
                string data = string.Format(format, string.Format("Wname{0}", i));
                _store.LoadFromString(data, new TriGParser());
            }

            Debug.WriteLine("Loaded {0} quads", _store.Triples.Count());
            _store.SaveToFile("data.trig", new TriGWriter());
        }

        private IEnumerable TimedDatasets()
        {
            return Enumerable.Range(1, 20).Select(i => new TestCaseData(i * 3));
        }

        private IEnumerable Queries()
        {
            ////yield return new TestCaseData(Resourcer.Resource.AsString("Queries.Original.rq")).SetName("Original query");
            ////yield return new TestCaseData(Resourcer.Resource.AsString("Queries.SPOSeparate.rq")).SetName("SPO in separate graph pattern");
            ////yield return new TestCaseData(Resourcer.Resource.AsString("Queries.NoRedundantPatterns.rq")).SetName("No redundant patterns");
            yield return new TestCaseData(Resourcer.Resource.AsString("Queries.NoSubquery.rq")).SetName("Subquery extracted");
            yield return new TestCaseData(Resourcer.Resource.AsString("Queries.AllOptimizations.rq")).SetName("All mixed");
        }
    }
}
